package com.devcamp.accountrestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> accountList() {
        ArrayList<Account> accounts = new ArrayList<>();
        Account account1 = new Account("abc123", "Nguyen Van A", 1000);
        Account account2 = new Account("abd123", "Nguyen Van B", 1500);
        Account account3 = new Account("abe123", "Nguyen Van C", 2000);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        System.out.println(account2.toString());
        System.out.println(account2.transferTo(account1, 1000));
        System.out.println(account2.toString());
        return accounts;

    }

}
