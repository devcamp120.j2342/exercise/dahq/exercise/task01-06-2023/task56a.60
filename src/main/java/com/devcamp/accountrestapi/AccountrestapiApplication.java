package com.devcamp.accountrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountrestapiApplication.class, args);
	}

}
