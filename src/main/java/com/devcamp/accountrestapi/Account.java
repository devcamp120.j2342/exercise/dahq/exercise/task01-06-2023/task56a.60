package com.devcamp.accountrestapi;

public class Account {
    private String id;
    private String name;
    private int balance = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }

    public int credit(int amount) {
        return this.balance + amount;
    }

    public int debit(int amount) {
        if (this.balance >= amount) {
            this.balance -= amount;
        } else {
            System.out.println("Amount exceddted to balance");
        }
        return this.balance;
    }

    public int transferTo(Account account, int amount) {
        if (this.balance >= amount) {
            this.balance = this.balance - amount;
            account.balance = account.balance + amount;
        } else {
            System.out.println("Amount excceted to balance");
        }
        return account.balance;

    }
}
